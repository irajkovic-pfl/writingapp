# The Most Dangerous Writing App
A desktop app. Allows a user to type and if they stop for more than 5 seconds, it will delete everything they've written so far.

User can save work any time, but must be quick :)

Tech: Python, GUI, Tkinter