import tkinter as tk

# Colors
color_primary = "#EEEDEB"
color_secondary = "#E0CCBE"
color_accent = "#747264"
color_text = "#3C3633"


# Timer
def timer_start():
    global timer
    window.after_cancel(timer)
    timer = window.after(10000, reset_app)


# Keypress event
def keypress(event):
    timer_start()


# Reset text when time expires
def reset_app():
    text.delete("1.0", tk.END)


# Save text on demand
def save_text():
    with open("writing.txt", "a", encoding="UTF-8") as file:
        txt = text.get("1.0", tk.END)
        if len(txt) > 1:
            file.write(txt)


# Config
window = tk.Tk()
window.title("Writing app")
window.config(pady=30, padx=30, bg=color_primary)
window.bind("<Key>", keypress)

# Timer
timer = window.after(10000, reset_app)

# Items
title_label = tk.Label(window, text="Written text will be erased if you stop for 10 seconds",
                       font=("Arial", 24, "bold"), pady=20, fg=color_text, bg=color_primary)
title_label.grid(column=0, row=0, columnspan=2)

text = tk.Text(window, height=20, width=100, font=("Arial", 16), padx=20, pady=20, bg=color_secondary, fg=color_text)
text.grid(column=0, row=1, columnspan=2)

save_button = tk.Button(window, text="Save", padx=10, pady=10, font=("Arial", 16), bg=color_accent, fg=color_primary, command=save_text)
save_button.grid(column=0, row=2)

exit_button = tk.Button(window, text="Exit", padx=10, pady=10, font=("Arial", 16), bg=color_accent, fg=color_primary, command=exit)
exit_button.grid(column=1, row=2)

text.focus()

# Loop
window.mainloop()
